<?php

use Illuminate\Database\Seeder;
use App\SalesOrder;
use App\Warehouse;

class SalesOrderWH extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sales_orders = SalesOrder::get();
        $warehouse = Warehouse::first();

        if ($sales_orders) {
        	foreach ($sales_orders as $item) {
        		$item->warehouse_id = $warehouse->id;
        		$item->save();
        	}
        }
    }
}
