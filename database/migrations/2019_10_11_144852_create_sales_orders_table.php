<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('so_number');
            $table->unsignedBigInteger('customer_id');
            $table->integer('quantity');
            $table->integer('volume');
            $table->date('delivery_date')->nullable();
            $table->string('description');
            $table->integer('status')->default(100);
            $table->timestamps();
        });

        Schema::table('sales_orders', function($table) {
            $table->foreign('customer_id')
                  ->references('id')->on('customers')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sales_orders', function (Blueprint $table) {
            $table->dropForeign('sales_orders_customer_id_foreign');
        });

        Schema::dropIfExists('sales_orders');
    }
}
