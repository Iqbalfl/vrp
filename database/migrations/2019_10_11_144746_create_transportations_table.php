<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransportationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transportations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('plate_number');
            $table->string('name');
            $table->unsignedBigInteger('warehouse_id');
            $table->string('contact');
            $table->integer('body_volume');
            $table->integer('duration')->nullable();
            $table->date('start_at')->nullable();
            $table->date('end_at')->nullable();
            $table->integer('status')->default(100);
            $table->timestamps();
        });

        Schema::table('transportations', function($table) {
            $table->foreign('warehouse_id')
                  ->references('id')->on('warehouses')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transportations', function (Blueprint $table) {
            $table->dropForeign('transportations_warehouse_id_foreign');
        });

        Schema::dropIfExists('transportations');
    }
}
