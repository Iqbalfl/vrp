<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoutingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('routings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('route');
            $table->float('total_distance',10,1)->nullable();
            $table->float('total_duration',10,1)->nullable();
            $table->unsignedInteger('total_volume')->nullable();
            $table->date('delivery_date');
            $table->unsignedBigInteger('transportation_id');
            $table->timestamps();

            $table->foreign('transportation_id')
                  ->references('id')->on('transportations')
                  ->onDelete('cascade');
        });

        Schema::create('routing_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('routing_id');
            $table->string('customer_name');
            $table->unsignedBigInteger('sales_order_id');
            $table->float('distance',8,1);
            $table->float('duration',8,1);
            $table->unsignedInteger('volume');
            $table->timestamps();

            $table->foreign('routing_id')
                  ->references('id')->on('routings')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('routings', function (Blueprint $table) {
            $table->dropForeign('routings_transportation_id_foreign');
        });

        Schema::table('routing_details', function (Blueprint $table) {
            $table->dropForeign('routing_details_routing_id_foreign');
        });

        Schema::dropIfExists('routings');
        Schema::dropIfExists('routing_details');
    }
}
