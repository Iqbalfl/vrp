<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoutingDetail extends Model
{
    public function routing()
    {
    	return $this->belongsTo('App\Routing');
    }

    public function salesOrder()
    {
    	return $this->belongsTo('App\SalesOrder', 'sales_order_id');
    }
}
