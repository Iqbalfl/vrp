<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Province;
use App\City;
use App\District;

class PlaceController extends Controller
{
    public function getCity($province_id)
    {
        $cities = City::where('province_id', $province_id)
                    ->pluck('name','id');

        return response()->json($cities);
    }

    public function getDistrict($city_id)
	{
		$districts = District::where('city_id',$city_id)
		     		->pluck('name','id');

		return response()->json($districts);
	}

}
