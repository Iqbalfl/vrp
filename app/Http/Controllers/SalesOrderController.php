<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Inventory;
use App\SalesOrder;
use Session;
use DB;

class SalesOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            $sales_orders = SalesOrder::with('customer');
            $sales_orders = $sales_orders->select('*');
            
            return Datatables::of($sales_orders)
                ->addIndexColumn()
                ->addColumn('action', function($sales_order){
                    return view('partials._action', [
                        'model'           => $sales_order,
                        'form_url'        => route('sales-order.destroy', $sales_order->id),
                        'edit_url'        => route('sales-order.edit', $sales_order->id),
                        'show_url'        => route('sales-order.show', $sales_order->id)
                    ]);
                })
                ->escapeColumns([])
                ->make(true);
        }

        return view('sales-orders.index'); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $so_number = SalesOrder::generateNumber();

        return view('sales-orders.create')->with(compact('so_number'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          'so_number' => 'required|string',
          'customer_id' => 'required|integer',
          'warehouse_id' => 'required|integer',
          'quantity' => 'required|integer',
          'volume' => 'required|integer',
          'delivery_date' => 'required|date',
          'description' => 'required|string',
        ]);

        DB::beginTransaction();
        
        $sales_order = new SalesOrder;
        $sales_order->so_number = $request->so_number;
        $sales_order->customer_id = $request->customer_id;
        $sales_order->warehouse_id = $request->warehouse_id;
        $sales_order->quantity = $request->quantity;
        $sales_order->volume = $request->volume;
        $sales_order->delivery_date = $request->delivery_date;
        $sales_order->description = $request->description;
        $sales_order->save();

        $inventory = Inventory::where('warehouse_id', $request->warehouse_id)->first();

        if ($inventory) {
            
            $left = $inventory->quantity - $request->quantity;

            if ($left < 0) {
                DB::rollback();

                Session::flash("flash_notification", [
                  "level"=>"warning",
                  "message"=>"Quantity inventory tidak mencukupi untuk order ini!"
                ]);
                return redirect()->back();
            }

            $inventory->quantity = $left;
            $inventory->save();

        } else {
            DB::rollback();

            Session::flash("flash_notification", [
              "level"=>"warning",
              "message"=>"Data Inventory belum tersedia, silahkan buat terlebih dahulu!"
            ]);
            return redirect()->back();
        }

        DB::commit();

        Session::flash("flash_notification", [
          "level"=>"success",
          "message"=>"Sales Order dengan kode $sales_order->so_number berhasil dibuat"
        ]);
        
        return redirect()->route('sales-order.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sales_order = SalesOrder::find($id);

        return view('sales-orders.show')->with(compact('sales_order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sales_order = SalesOrder::find($id);

        return view('sales-orders.edit')->with(compact('sales_order'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
          'so_number' => 'required|string',
          'customer_id' => 'required|integer',
          'warehouse_id' => 'required|integer',
          'quantity' => 'required|integer',
          'volume' => 'required|integer',
          'delivery_date' => 'required|date',
          'description' => 'required|string',
        ]);
        
        DB::beginTransaction();

        $sales_order = SalesOrder::find($id);
        $sales_order->so_number = $request->so_number;
        $sales_order->customer_id = $request->customer_id;
        $sales_order->warehouse_id = $request->warehouse_id;
        
        $data['statement'] = 'unknown';
        $data['difference'] = 0;
        $changes = false;

        if ($sales_order->quantity != $request->quantity) {
            
            $changes = true;

            if ($sales_order->quantity < $request->quantity) {
                $data['statement'] = 'minus';
                $data['difference'] = $request->quantity - $sales_order->quantity;
            } else {
                $data['statement'] = 'plus';
                $data['difference'] = $sales_order->quantity - $request->quantity;
            }
        }

        $sales_order->quantity = $request->quantity;

        $sales_order->volume = $request->volume;
        $sales_order->delivery_date = $request->delivery_date;
        $sales_order->description = $request->description;
        
        if ($request->status != null) {
            $sales_order->status = $request->status;
        }

        $sales_order->save();

        if ($changes == true) {
            $inventory = Inventory::where('warehouse_id', $request->warehouse_id)->first();

            if ($inventory) {

                if ($data['statement'] == 'minus') {
                    $new_qty = $inventory->quantity - $data['difference'];

                    if ($new_qty < 0) {
                        DB::rollback();

                        Session::flash("flash_notification", [
                          "level"=>"warning",
                          "message"=>"Quantity inventory tidak mencukupi untuk order ini!"
                        ]);
                        return redirect()->back();
                    }
                } else {
                    $new_qty = $inventory->quantity + $data['difference'];
                }
                
                $inventory->quantity = $new_qty;
                $inventory->save();

            } else {
                DB::rollback();

                Session::flash("flash_notification", [
                  "level"=>"warning",
                  "message"=>"Data Inventory belum tersedia, silahkan buat terlebih dahulu!"
                ]);
                return redirect()->back();
            }
        }

        DB::commit();

        Session::flash("flash_notification", [
          "level"=>"success",
          "message"=>"Sales Order dengan kode $sales_order->so_number berhasil diubah"
        ]);
        
        return redirect()->route('sales-order.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sales_order = SalesOrder::destroy($id);

        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Berhasil menghapus data"
        ]);

        return redirect()->route('sales-order.index');
    }
}
