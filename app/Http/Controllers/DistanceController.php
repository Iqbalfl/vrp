<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Customer;
use App\Warehouse;
use App\Distance;
use function BenTools\StringCombinations\string_combinations;
use Session;
use GuzzleHttp\Client;

class DistanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            $distances = Distance::with('origin', 'destination');
            $distances = $distances->select('*');
            
            return Datatables::of($distances)
                ->addIndexColumn()
                ->addColumn('action', function($distance){
                    return view('partials._action', [
                        'model'           => $distance,
                        'form_url'        => route('logistic.destroy', $distance->id)
                    ]);
                })
                ->escapeColumns([])
                ->make(true);
        }

        return view('distances.index'); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('distances.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          'origin_id' => 'required|string',
          'destination_id' => 'required|integer',
        ]);

        if (strpos($request->origin_id, 'wh') !== false) {
            $origin_type = 'App\Warehouse';
            $origin_id = substr($request->origin_id, 2);
            $origin = Warehouse::find($origin_id);
        } else {
            $origin_type = 'App\Customer';
            $origin_id = $request->origin_id;
            $origin = Customer::find($origin_id);
        }

        $destination_id = $request->destination_id;
        $destination = Customer::find($destination_id);

        // cek kemumgkinan persamaan origin dan destination
        if ($origin_type == 'App\Customer') {
            if ($origin_id == $destination_id) {
                Session::flash("flash_notification", [
                  "level"=>"warning",
                  "message"=>"Tidak bisa menambahkan distance dengan origin dan destination yang sama!"
                ]);
                return redirect()->back();
            }
        }

        // cek apakah data sudah ada
        $exist = false;

        $params = [
            0 => [
                'origin_id' => $origin_id,
                'destination_id' => $destination_id
            ],
            1 => [
                'origin_id' => $destination_id,
                'destination_id' => $origin_id
            ]
        ];

        foreach ($params as $param) {
            $check = Distance::where('origin_id', $param['origin_id'])->where('destination_id', $param['destination_id'])->where('origin_type', $origin_type)->first();
            if ($check) {
                $exist = true;
            }
        }

        if ($exist == true) {
            Session::flash("flash_notification", [
              "level"=>"warning",
              "message"=>"Data yang diinput sudah tersedia!"
            ]);
            return redirect()->back();
        }
    
        $client = new Client;
        $response = $client->request('GET', env('MAPS_API_DM_URL', 'PUT_YOUR_MAPS_API_DM_URL'), [
            'query' => [
                'origins' => $origin->lat.','.$origin->lng,
                'destinations' => $destination->lat.','.$destination->lng,
                'key' => env('MAPS_API_KEY', 'PUT_YOUR_API_KEY'),
            ],
        ]);

        $raw_distances = json_decode($response->getBody(), true);

        $distance_value = str_replace(' km', '', $raw_distances['rows'][0]['elements'][0]['distance']['text']);
        $duration_value = str_replace(' mins', '', $raw_distances['rows'][0]['elements'][0]['duration']['text']);

        $distance = new Distance;
        $distance->origin_id = $origin_id;
        $distance->origin_type = $origin_type;
        $distance->destination_id = $destination_id;
        $distance->distance = $distance_value;
        $distance->duration = $duration_value;
        $distance->save();

        Session::flash("flash_notification", [
          "level"=>"success",
          "message"=>"Data Distance berhasil disimpan"
        ]);

        return redirect()->route('logistic.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $distance = Distance::with('origin')->find($id);

        return response()->json($distance);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $distance = Distance::find($id);

        return view('distances.edit')->with(compact('distance'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $distance = Distance::destroy($id);

        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Berhasil menghapus data"
        ]);

        return redirect()->route('logistic.index');
    }
}
