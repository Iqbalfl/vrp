<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Warehouse;
use App\Customer;
use App\Transportation;
use App\SalesOrder;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = [
            'warehouse' => Warehouse::count(),
            'customer' => Customer::count(),
            'transportation' => Transportation::available()->count(),
            'sales_order' => SalesOrder::count(),
            'greeting' => $this->greeting()
        ];
        
        return view('home')->with(compact('data'));
    }

    public function greeting()
    {
        $greetings = "";

        $time = date("H");

        $timezone = date("e");

        if ($time < "12") {
            $greetings = "Selamat Pagi";
        } else

        if ($time >= "12" && $time < "17") {
            $greetings = "Selamat Siang";
        } else

        if ($time >= "17" && $time < "19") {
            $greetings = "Selamat Sore";
        } else

        if ($time >= "19") {
            $greetings = "Selamat Malam";
        }

        return $greetings;
    }
}
