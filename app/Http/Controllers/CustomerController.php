<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Customer;
use Session;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            $customers = Customer::query();
            $customers = $customers->select('*');
            
            return Datatables::of($customers)
                ->addIndexColumn()
                ->addColumn('action', function($customer){
                    return view('partials._action', [
                        'model'           => $customer,
                        'form_url'        => route('customer.destroy', $customer->id),
                        'edit_url'        => route('customer.edit', $customer->id),
                        'show_url'        => route('customer.show', $customer->id)
                    ]);
                })
                ->escapeColumns([])
                ->make(true);
        }

        return view('customers.index'); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('customers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          'name' => 'required|string|max:255',
          'type' => 'required|string',
          'address' => 'required|string',
          'district_id' => 'required|numeric',
          'city_id' => 'required|numeric',
          'province_id' => 'required|numeric',
          'phone' => 'required|numeric',
          'email' => 'required|string|email|max:255|unique:customers',
          'contact_person' => 'required|string',
          'lat' => 'required|string',
          'lng' => 'required|string',
        ]);
        
        $customer = new Customer;
        $customer->name = $request->name;
        $customer->type = $request->type;
        $customer->address = $request->address;
        $customer->district_id = $request->district_id;
        $customer->city_id = $request->city_id;
        $customer->province_id = $request->province_id;
        $customer->phone = $request->phone;
        $customer->email = $request->email;
        $customer->contact_person = $request->contact_person;
        $customer->lat = $request->lat;
        $customer->lng = $request->lng;
        $customer->save();

        Session::flash("flash_notification", [
          "level"=>"success",
          "message"=>"Customer dengan nama $customer->name berhasil dibuat"
        ]);
        
        return redirect()->route('customer.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $customer = Customer::find($id);

        return view('customers.show')->with(compact('customer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = Customer::find($id);

        return view('customers.edit')->with(compact('customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
          'name' => 'required|string|max:255',
          'type' => 'required|string',
          'address' => 'required|string',
          'phone' => 'required|numeric',
          'email' => 'required|string|email|max:255|unique:customers,email,' . $id,
          'contact_person' => 'required|string'
        ]);
        
        $customer = Customer::find($id);
        $customer->name = $request->name;
        $customer->type = $request->type;
        $customer->address = $request->address;

        if ($request->district_id != null && $request->city_id != null && $request->province_id != null) {
            $customer->district_id = $request->district_id;
            $customer->city_id = $request->city_id;
            $customer->province_id = $request->province_id;
        }

        $customer->phone = $request->phone;
        $customer->email = $request->email;
        $customer->contact_person = $request->contact_person;

        if ($request->lat != null && $request->lng != null) {
            $customer->lat = $request->lat;
            $customer->lng = $request->lng;
        }

        $customer->save();

        Session::flash("flash_notification", [
          "level"=>"success",
          "message"=>"Customer dengan nama $customer->name berhasil diubah"
        ]);
        
        return redirect()->route('customer.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = Customer::destroy($id);

        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Berhasil menghapus data"
        ]);

        return redirect()->route('customer.index');
    }
}
