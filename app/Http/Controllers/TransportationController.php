<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Transportation;
use Session;

class TransportationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            $transportations = Transportation::with('warehouse');
            $transportations = $transportations->select('*');
            
            return Datatables::of($transportations)
                ->addIndexColumn()
                ->addColumn('action', function($transportation){
                    return view('partials._action', [
                        'model'           => $transportation,
                        'form_url'        => route('transportation.destroy', $transportation->id),
                        'edit_url'        => route('transportation.edit', $transportation->id),
                        'show_url'        => route('transportation.show', $transportation->id)
                    ]);
                })
                ->escapeColumns([])
                ->make(true);
        }

        return view('transportations.index'); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('transportations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          'plate_number' => 'required|string',
          'name' => 'required|string|max:255',
          'warehouse_id' => 'required|integer',
          'contact' => 'required|string',
          'body_volume' => 'required|integer',
          'duration' => 'required|integer',
          'start_at' => 'required|date',
        ]);
        
        $transportation = new Transportation;
        $transportation->plate_number = $request->plate_number;
        $transportation->name = $request->name;
        $transportation->warehouse_id = $request->warehouse_id;
        $transportation->contact = $request->contact;
        $transportation->body_volume = $request->body_volume;
        $transportation->duration = $request->duration;
        $transportation->start_at = $request->start_at;
        $transportation->save();

        Session::flash("flash_notification", [
          "level"=>"success",
          "message"=>"Transportation dengan nama $transportation->name berhasil dibuat"
        ]);
        
        return redirect()->route('transportation.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transportation = Transportation::find($id);

        return view('transportations.show')->with(compact('transportation'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $transportation = Transportation::find($id);

        return view('transportations.edit')->with(compact('transportation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
          'plate_number' => 'required|string',
          'name' => 'required|string|max:255',
          'warehouse_id' => 'required|integer',
          'contact' => 'required|string',
          'body_volume' => 'required|integer',
          'duration' => 'required|integer',
          'start_at' => 'required|date',
          'status' => 'required|integer',
        ]);
        
        $transportation = Transportation::find($id);
        $transportation->plate_number = $request->plate_number;
        $transportation->name = $request->name;
        $transportation->warehouse_id = $request->warehouse_id;
        $transportation->contact = $request->contact;
        $transportation->body_volume = $request->body_volume;
        $transportation->duration = $request->duration;
        $transportation->start_at = $request->start_at;
        $transportation->status = $request->status;
        $transportation->save();

        Session::flash("flash_notification", [
          "level"=>"success",
          "message"=>"Transportation dengan nama $transportation->name berhasil diubah"
        ]);
        
        return redirect()->route('transportation.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $transportation = Transportation::destroy($id);

        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Berhasil menghapus data"
        ]);

        return redirect()->route('transportation.index');
    }
}
