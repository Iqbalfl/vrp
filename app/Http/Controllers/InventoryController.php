<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Inventory;
use Session;

class InventoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            $inventories = Inventory::with('warehouse');
            $inventories = $inventories->select('*');
            
            return Datatables::of($inventories)
                ->addIndexColumn()
                ->addColumn('action', function($inventory){
                    return view('partials._action', [
                        'model'           => $inventory,
                        'form_url'        => route('inventory.destroy', $inventory->id),
                        'edit_url'        => route('inventory.edit', $inventory->id)
                    ]);
                })
                ->escapeColumns([])
                ->make(true);
        }

        return view('inventories.index'); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('inventories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          'warehouse_id' => 'required|numeric',
          'quantity' => 'required|numeric',
        ]);

        $check = Inventory::where('warehouse_id', $request->warehouse_id)->first();
        
        if ($check) {
            Session::flash("flash_notification", [
              "level"=>"warning",
              "message"=>"Data inentory untuk warehouse ".$check->warehouse->name." sudah tersedia!"
            ]);
            
            return redirect()->back();
        }

        $inventory = new Inventory;
        $inventory->warehouse_id = $request->warehouse_id;
        $inventory->quantity = $request->quantity;
        $inventory->save();

        Session::flash("flash_notification", [
          "level"=>"success",
          "message"=>"Data berhasil di simpan"
        ]);
        
        return redirect()->route('inventory.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $inventory = Inventory::find($id);
        return view('inventories.edit')->with(compact('inventory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
          'warehouse_id' => 'required|numeric',
          'quantity' => 'required|numeric',
        ]);

        $inventory = Inventory::find($id);
        $inventory->warehouse_id = $request->warehouse_id;
        $inventory->quantity = $request->quantity;
        $inventory->save();

        Session::flash("flash_notification", [
          "level"=>"success",
          "message"=>"Data berhasil di ubah"
        ]);
        
        return redirect()->route('inventory.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $inventory = Inventory::destroy($id);

        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Berhasil menghapus data"
        ]);

        return redirect()->route('inventory.index');
    }
}
