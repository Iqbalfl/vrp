<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Transportation;
use App\SalesOrder;
use App\Distance;
use App\Customer;
use App\Warehouse;
use App\Routing;
use App\RoutingDetail;
use function BenTools\StringCombinations\string_combinations;
use Session;

class RoutingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            $routings = Routing::with('transportation');
            $routings = $routings->select('*');
            
            return Datatables::of($routings)
                ->addIndexColumn()
                ->addColumn('action', function($routing){
                    return view('partials._action', [
                        'model'           => $routing,
                        'form_url'        => route('routing.destroy', $routing->id),
                        'show_url'        => route('routing.show', $routing->id)
                    ]);
                })
                ->escapeColumns([])
                ->make(true);
        }

        return view('routings.index'); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('routings.create'); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          'transportation_id' => 'required|integer',
          'so_id' => 'required|array',
        ]);

        $transportation = Transportation::find($request->transportation_id);
        $sales_orders = SalesOrder::whereIn('id', $request->so_id)->get();
        $cs_ids = $sales_orders->pluck('customer_id');
        $warehouse_id = $transportation->warehouse_id;

        $array_id = array_unique($cs_ids->values()->all());
        $size = count($array_id);
        $str = implode('', $array_id);

        if ($size > 5) {
            Session::flash("flash_notification", [
              "level"=>"warning",
              "message"=>"Tidak bisa memproses routing lebih dari 5 customer!"
            ]);
            return redirect()->back();
        }

        $distanceCust = Distance::where('origin_type', 'App\Customer')->get();
        $distanceWh = Distance::where('origin_type', 'App\Warehouse')->get();
        $warehouse = Warehouse::find($warehouse_id);

        // kombinasikan semua kemungkinan string 
        $combinations = string_combinations($str, $min = $size, $max = $size);
        $combinations = $combinations->withoutDuplicates()->asArray();

        // split string jadi 2 character
        $arrs = [];
        foreach ($combinations as $combination) {
            $arr_string = str_split($combination);
            $count_arr = count($arr_string);
            foreach ($arr_string as $key => $str) {
                if ($key < $count_arr-1){
                    $arrs[$combination][$key] = substr($combination, $key, 2);  
                }
            }
        }

        // dapatkan data jarak,durasi antar store kemudian hitung
        $array_distance_tmp = [];
        foreach ($arrs as $key => $arr) {
            $value_dis = 0;
            $value_dur = 0;
            foreach ($arr as $id) {
                $p1 = $distanceCust->where('origin_id', $id[0])->where('destination_id', $id[-1])->first();
                if ($p1 != null) {
                    $tmp['distance'] = $p1->distance;
                    $tmp['duration'] = $p1->duration;
                } else {
                    $p2 = $distanceCust->where('origin_id', $id[-1])->where('destination_id', $id[0])->first();
                    if ($p2 != null) {
                        $tmp['distance'] = $p2->distance;
                        $tmp['duration'] = $p2->duration;
                    } else {
                        Session::flash("flash_notification", [
                          "level"=>"warning",
                          "message"=>"Tidak bisa memproses routing karena terdapat data distance yang belum lengkap!"
                        ]);
                        return redirect()->back();
                    }
                }
                $value_dis += $tmp['distance'];
                $value_dur += $tmp['duration'];
                $array_distance_tmp[$key]['distance'] = $value_dis;
                $array_distance_tmp[$key]['duration'] = $value_dur;
            }
        }

        // buat text rute
        $arr_route = [];
        foreach ($combinations as $cmb) {
            $arr_string = str_split($cmb);
            $tmp_route = '';
            foreach ($arr_string as $key => $id) {
                $tmp_route .= ' --> '.Customer::find($id)->name;
            }
            $arr_route[$cmb] = $warehouse->name.$tmp_route; 
        }

        // dapatkan data jarak,durasi dari warehouse ke store pertama
        $tmp_cb = [];
        foreach ($combinations as $key => $item) {
            $xy = $distanceWh->where('origin_id', $warehouse_id)->where('destination_id', $item[0])->first();
            if ($xy != null) {
                $tmp_cb[$item]['distance'] = $xy->distance;
                $tmp_cb[$item]['duration'] = $xy->duration;
            } else {
                Session::flash("flash_notification", [
                  "level"=>"warning",
                  "message"=>"Tidak bisa memproses routing karena terdapat data distance yang belum lengkap!"
                ]);
                return redirect()->back();
            }
        }

        // gabungkan array data jarak, durasi dan rute
        foreach ($arr_route as $key => $route) {
            $array_distance_tmp[$key]['distance'] = $tmp_cb[$key]['distance'] + $array_distance_tmp[$key]['distance'];
            $array_distance_tmp[$key]['duration'] = $tmp_cb[$key]['duration'] + $array_distance_tmp[$key]['duration'];
            $array_distance_tmp[$key]['route'] = $route;
        }

        // rubah array jadi collection, kemudian ambil rute terdekat
        $final_distance = collect($array_distance_tmp)->sortBy('distance');
        $data = $final_distance->first();

        // simpan data ke database
        $routing = new Routing;
        $routing->route = $data['route'];
        $routing->total_distance = $data['distance'];
        $routing->total_duration = $data['duration'];
        $routing->delivery_date = now()->toDateString();
        $routing->transportation_id = $request->transportation_id;
        $routing->save();

        // buat roting details
        $total_volume = 0;
        foreach ($sales_orders as $so) {
            $detail = new RoutingDetail;
            $detail->routing_id = $routing->id;
            $detail->customer_name = $so->customer->name;
            $detail->sales_order_id = $so->id;
            $detail->volume = $so->volume;
            $detail->save();

            // rubah status sales order menjadi finish
            $so->status = 200;
            $so->save();

            $total_volume += $detail->volume;
        }

        // rubah status transportasi menjadi not available
        $transportation->status = 10;
        $transportation->save();

        // simpan total volume
        $routing->total_volume = $total_volume;
        $routing->save();

        Session::flash("flash_notification", [
          "level"=>"success",
          "message"=>"Berhasil membuat rute!"
        ]);
        return redirect()->route('routing.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $routing = Routing::find($id);

        return view('routings.show')->with(compact('routing'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $routing = Routing::destroy($id);

        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Berhasil menghapus data"
        ]);

        return redirect()->route('routing.index');
    }
}
