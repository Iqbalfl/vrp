<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Warehouse;
use Session;

class WarehouseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            $warehouses = Warehouse::query();
            $warehouses = $warehouses->select('*');
            
            return Datatables::of($warehouses)
                ->addIndexColumn()
                ->addColumn('action', function($warehouse){
                    return view('partials._action', [
                        'model'           => $warehouse,
                        'form_url'        => route('warehouse.destroy', $warehouse->id),
                        'edit_url'        => route('warehouse.edit', $warehouse->id)
                    ]);
                })
                ->escapeColumns([])
                ->make(true);
        }

        return view('warehouses.index'); 
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('warehouses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          'name' => 'required|string|max:255',
          'address' => 'required|string',
          'phone' => 'required|numeric',
          'email' => 'required|string|email|max:255|unique:warehouses',
          'contact_person' => 'required|string',
          'lat' => 'required|string',
          'lng' => 'required|string',
        ]);
        
        $warehouse = new Warehouse;
        $warehouse->name = $request->name;
        $warehouse->address = $request->address;
        $warehouse->phone = $request->phone;
        $warehouse->email = $request->email;
        $warehouse->contact_person = $request->contact_person;
        $warehouse->lat = $request->lat;
        $warehouse->lng = $request->lng;
        $warehouse->save();

        Session::flash("flash_notification", [
          "level"=>"success",
          "message"=>"Warehouse dengan nama $warehouse->name berhasil dibuat"
        ]);
        
        return redirect()->route('warehouse.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $warehouse = Warehouse::find($id);

        return view('warehouses.edit')->with(compact('warehouse'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
          'name' => 'required|string|max:255',
          'address' => 'required|string',
          'phone' => 'required|numeric',
          'email' => 'required|string|email|max:255|unique:warehouses,email,' . $id,
          'contact_person' => 'required|string'
        ]);
        
        $warehouse = Warehouse::find($id);
        $warehouse->name = $request->name;
        $warehouse->address = $request->address;
        $warehouse->phone = $request->phone;
        $warehouse->email = $request->email;
        $warehouse->contact_person = $request->contact_person;

        if ($request->lat != null && $request->lng != null) {
            $warehouse->lat = $request->lat;
            $warehouse->lng = $request->lng;
        }

        $warehouse->save();

        Session::flash("flash_notification", [
          "level"=>"success",
          "message"=>"Warehouse dengan nama $warehouse->name berhasil diubah"
        ]);
        
        return redirect()->route('warehouse.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $warehouse = Warehouse::destroy($id);

        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Berhasil menghapus data"
        ]);

        return redirect()->route('warehouse.index');
    }
}
