<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\SalesOrder;
use App\Inventory;
use App\Routing;
use Session;

class ManagerController extends Controller
{
    public function index(Request $request)
    {
    	 if ($request->ajax()) {

            $routings = Routing::with('details.salesOrder');
            $routings = $routings->select('*');
            
            return Datatables::of($routings)
                ->addIndexColumn()
                ->addColumn('action', function($routing){
                    return view('partials._action', [
                        'model'           => $routing,
                        'show_url'        => route('routing.show', $routing->id)
                    ]);
                })
                ->addColumn('total_qty', function($routing){
                	$total_qty = 0;
                    foreach ($routing->details as $item) {
                    	$total_qty += $item->salesOrder->quantity;
                    }
                    return $total_qty;
                })
                ->escapeColumns([])
                ->make(true);
        }

    	return view('managers.index');
    }
}
