<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\User;
use Session;

class UserController extends Controller
{
    public function showProfile()
    {
        $user = \Auth::user();

        return view('users.profile', ['user' => $user]);
    }

    public function updateProfile(Request $request)
    {
        $auth = \Auth::user()->id;
        $user = User::find($auth);
        
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|unique:users,email,' . $auth,
            'avatar' => 'image|max:1024',
        ]);

        if ($request->hasFile('avatar')) {
            // megnambil image yang diupload berikut ekstensinya
            $filename = null;
            $uploaded_image = $request->file('avatar');
            $extension = $uploaded_image->getClientOriginalExtension();
            // membuat nama file random dengan extension
            $filename = md5(time()) . '.' . $extension;
            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'avatars';
            // memindahkan file ke folder public/images
            $uploaded_image->move($destinationPath, $filename);
            // hapus image lama, jika ada
            if ($user->avatar) {
                $old_image = $user->avatar;
                $filepath = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'avatars' . DIRECTORY_SEPARATOR . $user->avatar;
                try {
                    File::delete($filepath);
                } catch (FileNotFoundException $e) {
                    // File sudah dihapus/tidak ada
                    }
            }
            // ganti field image dengan image yang baru
            $user->avatar = $filename;
        }

        $user->name = $request->name;  
        $user->email = $request->email;        
        $user->save();
    
        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Profil berhasil diubah"
        ]);

        return redirect()->route('profile.show');
    }
}
