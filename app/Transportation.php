<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transportation extends Model
{
	protected $appends = ['display_status'];

    public function getDisplayStatusAttribute()
    {   
    	$value = $this->status;
        if ($value == 100) {
            $status = "Available";
        } else if ($value == 10) {
            $status = "Not Available";
        } else {
            $status = "Unknown";
        }
        
        return $status;
    }

    public function warehouse()
    {
    	return $this->belongsTo('App\Warehouse');
    }

    public function scopeAvailable($query)
    {
        return $query->where('status', 100);
    }
}
