<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Routing extends Model
{
    public function transportation()
    {
    	return $this->belongsTo('App\Transportation');
    }

    public function details()
    {
    	return $this->hasMany('App\RoutingDetail');
    }
}
