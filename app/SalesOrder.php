<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesOrder extends Model
{
	protected $appends = ['display_status'];

    public function getDisplayStatusAttribute()
    {   
    	$value = $this->status;
        if ($value == 100) {
            $status = "Available";
        } else if ($value == 200) {
            $status = "Finished";
        } else if ($value == 10) {
            $status = "Not Available";
        } else {
            $status = "Unknown";
        }
        
        return $status;
    }
    
    public function customer()
    {
    	return $this->belongsTo('App\Customer');
    }

    public function warehouse()
    {
        return $this->belongsTo('App\Warehouse');
    }

    public function scopeGenerateNumber($query)
    {
        $unique = false;
        while ($unique == false) {
            $randomID = 'SO-'.now()->format('ym').'-'.rand(1000, 9999);
            $check = $query->where('so_number',$randomID)->count();
            if ($check > 0) {
              $unique = false;
            } else {
              $unique = true;
            }
        }
        return $randomID;    
    }

    public function scopeAvailable($query)
    {
        return $query->where('status', 100);
    }
}
