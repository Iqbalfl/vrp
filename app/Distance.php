<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Distance extends Model
{
    /**
     * Get the owning origin model.
     */
    public function origin()
    {
        return $this->morphTo();
    }

    public function destination()
    {
    	return $this->belongsTo('App\Customer', 'destination_id');
    }
}
