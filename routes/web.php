<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'main', 'middleware'=>'auth'], function () {
	Route::resource('/warehouse', 'WarehouseController');
	Route::resource('/inventory', 'InventoryController');
	Route::resource('/customer', 'CustomerController');
	Route::resource('/transportation', 'TransportationController');
	Route::resource('/sales-order', 'SalesOrderController');
	Route::resource('/logistic', 'DistanceController');
	Route::resource('/routing', 'RoutingController');
	Route::resource('/store', 'StoreController');

	Route::get('/manager', 'ManagerController@index')->name('manager.index');

	Route::get('/user/profile', 'UserController@showProfile')->name('profile.show');
	Route::put('/user/profile', 'UserController@updateProfile')->name('profile.update');

	Route::get('/place/city/{province_id}', 'PlaceController@getCity')->name('city.list');
	Route::get('/place/district/{city_id}', 'PlaceController@getDistrict')->name('district.list');
});