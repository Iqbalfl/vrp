@extends('layouts.main')

@section('css')
  <style type="text/css">
    #geomap{
        width: 100%;
        height: 400px;
    }
  </style>
@endsection

@section('content')
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Customer</h1>
  </div>

  <div class="row">
    <div class="col-lg-12">

      <!-- Basic Card Example -->
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Tambah Customer</h6>
        </div>
        <div class="card-body">
          
          <form method="POST" action="{{ route('customer.store') }}" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="row">
              <div class="col-lg-6">
                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                  <label for="name">Nama Customer</label>
                  <input id="name" type="text" class="form-control @if ($errors->has('name')) is-invalid @endif" name="name" value="{{ old('name') }}">
                  @if ($errors->has('name'))
                    <div class="invalid-feedback">
                      {{ $errors->first('name') }}
                    </div>
                  @endif
                </div>
              </div>
              <div class="col-lg-6">
                <div class="form-group {{ $errors->has('type') ? ' has-error' : '' }}">
                  <label for="type">Jenis</label>
                  <input id="type" type="text" class="form-control @if ($errors->has('type')) is-invalid @endif" name="type" value="{{ old('type') }}">
                  @if ($errors->has('type'))
                    <div class="invalid-feedback">
                      {{ $errors->first('type') }}
                    </div>
                  @endif
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-lg-6">
                <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
                  <label for="phone">Nomor Telepon</label>
                  <input id="phone" type="number" class="form-control @if ($errors->has('phone')) is-invalid @endif" name="phone" value="{{ old('phone') }}">
                  @if ($errors->has('phone'))
                    <div class="invalid-feedback">
                      {{ $errors->first('phone') }}
                    </div>
                  @endif
                </div>
              </div>
              <div class="col-lg-6">
                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                  <label for="email">Email</label>
                  <input id="email" type="email" class="form-control @if ($errors->has('email')) is-invalid @endif" name="email" value="{{ old('email') }}">
                  @if ($errors->has('email'))
                    <div class="invalid-feedback">
                      {{ $errors->first('email') }}
                    </div>
                  @endif
                </div>
              </div>
            </div>

            <div class="form-group {{ $errors->has('contact_person') ? ' has-error' : '' }}">
              <label for="contact_person">Contact Person</label>
              <input id="contact_person" type="text" class="form-control @if ($errors->has('contact_person')) is-invalid @endif" name="contact_person" value="{{ old('contact_person') }}">
              @if ($errors->has('contact_person'))
                <div class="invalid-feedback">
                  {{ $errors->first('contact_person') }}
                </div>
              @endif
            </div>

            <div class="form-group {{ $errors->has('address') ? ' has-error' : '' }}">
              <label for="address">Alamat</label>
              <input id="address" type="text" class="form-control @if ($errors->has('address')) is-invalid @endif" name="address" value="{{ old('address') }}">
              @if ($errors->has('address'))
                <div class="invalid-feedback">
                  {{ $errors->first('address') }}
                </div>
              @endif
            </div>

            <div class="form-group {{ $errors->has('province_id') ? ' has-error' : '' }}">
              <label for="province_id">Provinsi</label>
              <select name="province_id" class="form-control" id="select-province">
                <option></option>
                @foreach (\App\Province::all() as $item)
                  <option value="{{$item->id}}">{{$item->name}}</option>
                @endforeach
              </select>
              @if ($errors->has('province_id'))
                <div class="invalid-feedback">
                  {{ $errors->first('province_id') }}
                </div>
              @endif
            </div>

            <div class="form-group {{ $errors->has('city_id') ? ' has-error' : '' }}">
              <label for="city_id">Kota / Kabupaten</label>
              <select name="city_id" class="form-control" id="select-city">
                <option></option>
              </select>
              @if ($errors->has('city_id'))
                <div class="invalid-feedback">
                  {{ $errors->first('city_id') }}
                </div>
              @endif
            </div>

            <div class="form-group {{ $errors->has('district_id') ? ' has-error' : '' }}">
              <label for="district_id">Kecamatan</label>
              <select name="district_id" class="form-control" id="select-district">
                <option></option>
              </select>
              @if ($errors->has('district_id'))
                <div class="invalid-feedback">
                  {{ $errors->first('district_id') }}
                </div>
              @endif
            </div>

            <label for="address">Cari Lokasi / (Geser Marker di Maps)</label>
            <div class="input-group mb-4">
              <input type="text" id="search_location" class="form-control">
              <div class="input-group-append">
                <button class="btn btn-primary get_map">Cari</button>
              </div>
            </div>

            <div id="geomap" data-height="400" class="mb-4"></div>

            <div class="row">
              <div class="col-lg-6">
                <div class="form-group {{ $errors->has('lat') ? ' has-error' : '' }}">
                  <label for="lat">Latitude</label>
                  <input id="input_latitude" type="text" class="form-control @if ($errors->has('lat')) is-invalid @endif" name="lat" tabindex="1" value="{{ old('lat') }}" readonly="true">
                  @if ($errors->has('lat'))
                    <div class="invalid-feedback">
                      {{ $errors->first('lat') }}
                    </div>
                  @endif
                </div>
              </div>
              <div class="col-lg-6">
                <div class="form-group {{ $errors->has('lng') ? ' has-error' : '' }}">
                  <label for="lng">Longitude</label>
                  <input id="input_longitude" type="text" class="form-control @if ($errors->has('lng')) is-invalid @endif" name="lng" tabindex="1" value="{{ old('lng') }}" readonly="true">
                  @if ($errors->has('lng'))
                    <div class="invalid-feedback">
                      {{ $errors->first('lng') }}
                    </div>
                  @endif
                </div>
              </div>
            </div>

            <div class="form-group">
              <button type="submit" class="btn btn-primary btn-block" tabindex="4">
                Simpan
              </button>
            </div>
          </form>

        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
  <script type="text/javascript">
    $(document).ready(function() {
      $('#select-province').select2({
        placeholder: "Pilih Provinsi"
      });

      $('#select-city').select2({
        placeholder: "Pilih Kota / Kabupaten"
      });

      $('#select-district').select2({
        placeholder: "Pilih Kecamatan"
      });
    });

    $(document).ready(function() {
      $('#select-province').on('change', function() {
        var province_id = $(this).val();
        if(province_id) {
          $.ajax({
              url: '{{ url('/main/place/city') }}'+'/'+province_id,
              type: "GET",
              dataType: "json",
              success:function(data)
              {
                $('#select-city').empty();
                $('#select-district').empty();
                $.each(data, function(key, value) {
                    $('#select-city').append('<option value="'+ key +'">'+ value +'</option>');
                });
              }
          });
        } else {
          $('#select-city').empty();
        }
      });
    });

    $(document).ready(function() {
      $('#select-city').on('change', function() {
        var province_id = $(this).val();
        if(province_id) {
          $.ajax({
              url: '{{ url('/main/place/district') }}'+'/'+province_id,
              type: "GET",
              dataType: "json",
              success:function(data)
              {
                $('#select-district').empty();
                $.each(data, function(key, value) {
                    $('#select-district').append('<option value="'+ key +'">'+ value +'</option>');
                });
              }
          });
        } else {
          $('#select-district').empty();
        }
      });
    });

  </script>

  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?libraries=places&key={{ env("MAPS_API_KEY", "PUT_YOUR_API_KEY") }}"></script>
  <script type="text/javascript" src="{{ asset('vendor/sb-temp/vendor/maps/gmaps.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/maps-config.js') }}"></script>
@endsection