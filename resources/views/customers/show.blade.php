@extends('layouts.main')

@section('content')
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Customer</h1>
  </div>

  <div class="row">
    <div class="col-lg-12">

      <!-- Basic Card Example -->
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Lihat Detail Customer</h6>
        </div>
        <div class="card-body">
          
          <p>
            Nama: <br><strong>{{$customer->name}}</strong>
            <hr>
          </p>
          <p>
            Jenis: <br><strong>{{$customer->type}}</strong>
            <hr>
          </p>
          <p>
            Alamat: <br><strong>{{$customer->address}}</strong>
            <hr>
          </p>
          <p>
            Kecamatan: <br><strong>{{$customer->district->name}}</strong>
            <hr>
          </p>
          <p>
            Kota / Kabupaten: <br><strong>{{$customer->city->name}}</strong>
            <hr>
          </p>
          <p>
            Provinsi: <br><strong>{{$customer->province->name}}</strong>
            <hr>
          </p>
          <p>
            Nomor Telepon: <br><strong>{{$customer->phone}}</strong>
            <hr>
          </p>
          <p>
            Email: <br><strong>{{$customer->email}}</strong>
            <hr>
          </p>
          <p>
            Contact Person: <br><strong>{{$customer->contact_person}}</strong>
            <hr>
          </p>

        </div>
      </div>
    </div>
  </div>
@endsection