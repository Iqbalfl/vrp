<div class="sidebar col-md-4 col-sm-4">
  <div class="widget clearfix">
    <h4 class="widget-title"><i class="far fa-envelope"></i> Berlangganan email</h4>
    <div class="newsletter-widget">
      <p>Dapatkan informasi terbaru dari kami langsung ke email anda.</p>
      <form class="form-inline" role="search">
        <div class="form-1">
          <input type="text" class="form-control" placeholder="Masukan email disini..">
          <button type="submit" class="btn btn-primary"><i class="far fa-envelope"></i></button>
        </div>
      </form>
    </div><!-- end newsletter -->
  </div><!-- end widget -->

  <div class="widget clearfix">
    <h4 class="widget-title"><i class="fa fa-clipboard-list"></i> Post Terbaru</h4>
    <div class="category-widget">
      @foreach (\App\Post::latestPost(4)->get() as $post)
        <div class="media">
          <div class="media-left">
            <a href="{{ route('fe.post.show', $post->slug) }}">
              <img class="media-object" src="{{ asset('vendor/frontend/upload/blog_04.jpg') }}" alt="..." height="64px">
            </a>
          </div>
          <div class="media-body">
            <h4 class="media-heading">{{ $post->title }}</h4>
            <div class="blog-meta">
              <ul class="list-inline">
                <li><a href="#"><i class="far fa-user"></i> {{ $post->user->name }}</a></li>
                <li><i class="far fa-eye"></i> {{ $post->view_count }}</li>
              </ul>
            </div>
          </div>
        </div>
      @endforeach

    </div><!-- end category -->
  </div><!-- end widget -->

  <div class="widget clearfix">
    <h4 class="widget-title"><i class="far fa-folder-open"></i> Post Kategori</h4>
    <div class="category-widget">
      <ul>
        <li><a href="{{ route('fe.post').'?category=all' }}"><i class="far fa-arrow-alt-circle-right"></i> Semua</a></li>
        @foreach (\App\Category::active()->get() as $category)
          <li><a href="{{ route('fe.post').'?category='.$category->slug }}"><i class="far fa-arrow-alt-circle-right"></i> {{ $category->name }}</a></li>
        @endforeach
      </ul>
    </div><!-- end category -->
  </div><!-- end widget -->

</div><!-- end col -->