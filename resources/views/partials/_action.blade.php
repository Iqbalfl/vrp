<form class="delete" action="{{ $form_url ?? '' }}" method="post" id="delete-form">
    {{csrf_field()}}
    {{method_field('delete')}}
    @isset ($show_url)
        <a href="{{ $show_url }}" class="btn btn-circle btn-sm btn-info" title="Lihat Detail Data"><i class="fa fa-eye"></i></a>
    @endisset
    @isset ($edit_url)
        <a href="{{ $edit_url }}" class="btn btn-circle btn-sm btn-warning" title="Edit Data"><i class="fa fa-edit"></i></a>
    @endisset
    @isset ($form_url)
        <button type="submit" value="Delete" class="btn btn-circle btn-sm btn-danger js-submit-confirm" title="Hapus Data">
            <i class="fa fa-trash"></i>
        </button>
    @endisset
</form>