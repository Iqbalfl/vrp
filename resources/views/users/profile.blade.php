@extends('layouts.main')

@section('content')
<div class="row">
    <div class="col-lg-12">

      <!-- Basic Card Example -->
        <div class="card card-success shadow">
          <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold">Edit Profil</h6>
          </div>
          <div class="card-body">

            <form method="POST" action="{{ route('profile.update', $user->id) }}" enctype="multipart/form-data">
              @csrf
              @method('PUT')

              <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="name">Nama Lengkap</label>
                <input id="name" type="text" class="form-control @if ($errors->has('name')) is-invalid @endif" name="name" tabindex="1" value="{{ $user->name }}">
                @if ($errors->has('name'))
                  <div class="invalid-feedback">
                    {{ $errors->first('name') }}
                  </div>
                @endif
              </div>
              
              <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email">Email</label>
                <input id="email" type="email" class="form-control @if ($errors->has('email')) is-invalid @endif" name="email" tabindex="1" value="{{ $user->email }}">
                @if ($errors->has('email'))
                  <div class="invalid-feedback">
                    {{ $errors->first('email') }}
                  </div>
                @endif
              </div>

              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="avatar">Avatar</label>
                @if (is_null($user->avatar))
                  <img src="{{asset('vendor/sb-temp/img/avatar-1.jpg')}}" class="rounded d-block" width="200" alt="avatar">
                @else
                  <img src="{{asset('uploads/images/avatars/'.$user->avatar)}}" class="rounded d-block" width="200" alt="avatar">
                @endif
                <br>
                <i class="small">** Maximum size gambar adalah 1MB</i><br>
                <i class="small">** Disarankan upload gambar dengan rasio 1:1 / persegi</i>
              </div>

              <div class="form-group{{ $errors->has('avatar') ? ' has-error' : '' }} custom-file mb-3">
                <input id="avatar" type="file" class="custom-file-input @if ($errors->has('avatar')) is-invalid @endif" name="avatar" tabindex="1">
                <label class="custom-file-label" for="customFile">Pilih Gambar</label>
                @if ($errors->has('avatar'))
                  <div class="invalid-feedback">
                    {{ $errors->first('avatar') }}
                  </div>
                @endif
              </div>

              <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block" tabindex="4">
                  Simpan
                </button>
              </div>
            </form>

          </div>
        </div>

    </div>
  </div>
@endsection
