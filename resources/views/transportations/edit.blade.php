@extends('layouts.main')

@section('content')
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Moda Transportasi</h1>
  </div>

  <div class="row">
    <div class="col-lg-12">

      <!-- Basic Card Example -->
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Edit Moda Transportasi</h6>
        </div>
        <div class="card-body">
          
          <form method="POST" action="{{ route('transportation.update', $transportation->id) }}" enctype="multipart/form-data">
            @csrf
            @method('PUT')

            <div class="form-group {{ $errors->has('plate_number') ? ' has-error' : '' }}">
              <label for="plate_number">Plat Nomor</label>
              <input id="plate_number" type="text" class="form-control @if ($errors->has('plate_number')) is-invalid @endif" name="plate_number" tabindex="1" value="{{ $transportation->plate_number }}">
              @if ($errors->has('plate_number'))
                <div class="invalid-feedback">
                  {{ $errors->first('plate_number') }}
                </div>
              @endif
            </div>

            <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
              <label for="name">Nama</label>
              <input id="name" type="text" class="form-control @if ($errors->has('name')) is-invalid @endif" name="name" tabindex="1" value="{{ $transportation->name }}">
              @if ($errors->has('name'))
                <div class="invalid-feedback">
                  {{ $errors->first('name') }}
                </div>
              @endif
            </div>

            <div class="form-group {{ $errors->has('warehouse_id') ? ' has-error' : '' }}">
              <label for="warehouse_id">Warehouse</label>
              <select id="warehouse_id" class="form-control @if ($errors->has('warehouse_id')) is-invalid @endif" name="warehouse_id" tabindex="1">
                <option value=""></option>
                @foreach (\App\Warehouse::all() as $item)
                  <option value="{{ $item->id }}" @if ($transportation->warehouse_id == $item->id) selected @endif>{{ $item->name }}</option>
                @endforeach
              </select>
              @if ($errors->has('warehouse_id'))
                <div class="invalid-feedback">
                  {{ $errors->first('warehouse_id') }}
                </div>
              @endif
            </div>

            <div class="form-group {{ $errors->has('contact') ? ' has-error' : '' }}">
              <label for="contact">Contact</label>
              <input id="contact" type="tex'" class="form-control @if ($errors->has('contact')) is-invalid @endif" name="contact" tabindex="1" value="{{ $transportation->contact }}">
              @if ($errors->has('contact'))
                <div class="invalid-feedback">
                  {{ $errors->first('contact') }}
                </div>
              @endif
            </div>

            <div class="form-group {{ $errors->has('body_volume') ? ' has-error' : '' }}">
              <label for="body_volume">Volume Karoseri</label>
              <div class="input-group mb-2">
                <input id="body_volume" type="number" class="form-control @if ($errors->has('body_volume')) is-invalid @endif" name="body_volume" tabindex="1" value="{{ $transportation->body_volume }}">
                <div class="input-group-append">
                  <div class="input-group-text">cm<sup>3</sup></div>
                </div>
              </div>
              @if ($errors->has('body_volume'))
                <div class="invalid-feedback">
                  {{ $errors->first('body_volume') }}
                </div>
              @endif
            </div>

            <div class="form-group {{ $errors->has('duration') ? ' has-error' : '' }}">
              <label for="duration">Durasi</label>
              <div class="input-group mb-2">
                <input id="duration" type="number" class="form-control @if ($errors->has('duration')) is-invalid @endif" name="duration" tabindex="1" value="{{ $transportation->duration }}">
                <div class="input-group-append">
                  <div class="input-group-text">hari</div>
                </div>
              </div>
              @if ($errors->has('duration'))
                <div class="invalid-feedback">
                  {{ $errors->first('duration') }}
                </div>
              @endif

            <div class="form-group {{ $errors->has('start_at') ? ' has-error' : '' }}">
              <label for="start_at">Start Date</label>
              <input id="start_at" type="date" class="form-control @if ($errors->has('start_at')) is-invalid @endif" name="start_at" tabindex="1" value="{{ $transportation->start_at }}">
              @if ($errors->has('start_at'))
                <div class="invalid-feedback">
                  {{ $errors->first('start_at') }}
                </div>
              @endif
            </div>

            <div class="form-group {{ $errors->has('status') ? ' has-error' : '' }}">
              <label for="status">Status</label>
              <select id="status" class="form-control @if ($errors->has('status')) is-invalid @endif" name="status" tabindex="1">
                <option value="10" @if ($transportation->status == 10) selected @endif>Not Available</option>
                <option value="100" @if ($transportation->status == 100) selected @endif>Available</option>
              </select>
              @if ($errors->has('status'))
                <div class="invalid-feedback">
                  {{ $errors->first('status') }}
                </div>
              @endif
            </div>

            <div class="form-group">
              <button type="submit" class="btn btn-primary btn-block" tabindex="1">
                Simpan
              </button>
            </div>
          </form>

        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
  <script type="text/javascript">
    $(document).ready(function() {
      $('#warehouse_id').select2({
        placeholder: "Pilih Warehouse"
      });
    });
  </script>
@endsection
