@extends('layouts.main')

@section('content')
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Moda Transportasi</h1>
  </div>

  <div class="row">
    <div class="col-lg-12">

      <!-- Basic Card Example -->
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Detail Moda Transportasi</h6>
        </div>
        <div class="card-body">
          
          <p>
            Plat Nomor: <br><strong>{{$transportation->plate_number}}</strong>
            <hr>
          </p>
          <p>
            Nama: <br><strong>{{$transportation->name}}</strong>
            <hr>
          </p>
          <p>
            Warehouse: <br><strong>{{$transportation->warehouse->name}}</strong>
            <hr>
          </p>
          <p>
            Contact: <br><strong>{{$transportation->contact}}</strong>
            <hr>
          </p>
          <p>
            Volume Karoseri: <br><strong>{{$transportation->body_volume}} cm<sup>3</sup></strong>
            <hr>
          </p>
          <p>
            Durasi: <br><strong>{{$transportation->duration}} hari</strong>
            <hr>
          </p>
          <p>
            Start from: <br><strong>{{$transportation->start_at}}</strong>
            <hr>
          </p>
          <p>
            End Date: <br><strong>{{$transportation->end_at ?? '-'}}</strong>
            <hr>
          </p>
          <p>
            Status: <br><strong>{{$transportation->display_status}}</strong>
            <hr>
          </p>

        </div>
      </div>
    </div>
  </div>
@endsection