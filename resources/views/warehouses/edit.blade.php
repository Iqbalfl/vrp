@extends('layouts.main')

@section('css')
  <style type="text/css">
    #geomap{
        width: 100%;
        height: 400px;
    }
  </style>
@endsection

@section('content')
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Warehouse</h1>
  </div>

  <div class="row">
    <div class="col-lg-12">

      <!-- Basic Card Example -->
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Edit Warehouse</h6>
        </div>
        <div class="card-body">
          
          <form method="POST" action="{{ route('warehouse.update', $warehouse->id) }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('PUT') }}

            <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
              <label for="name">Nama</label>
              <input id="name" type="text" class="form-control @if ($errors->has('name')) is-invalid @endif" name="name" tabindex="1" value="{{ $warehouse->name }}">
              @if ($errors->has('name'))
                <div class="invalid-feedback">
                  {{ $errors->first('name') }}
                </div>
              @endif
            </div>

            <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
              <label for="phone">Nomor Telepon</label>
              <input id="phone" type="text" class="form-control @if ($errors->has('phone')) is-invalid @endif" name="phone" tabindex="1" value="{{ $warehouse->phone }}">
              @if ($errors->has('phone'))
                <div class="invalid-feedback">
                  {{ $errors->first('phone') }}
                </div>
              @endif
            </div>

            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
              <label for="email">Email</label>
              <input id="email" type="email" class="form-control @if ($errors->has('email')) is-invalid @endif" name="email" tabindex="1" value="{{ $warehouse->email }}">
              @if ($errors->has('email'))
                <div class="invalid-feedback">
                  {{ $errors->first('email') }}
                </div>
              @endif
            </div>

            <div class="form-group {{ $errors->has('contact_person') ? ' has-error' : '' }}">
              <label for="contact_person">Contact Person</label>
              <input id="contact_person" type="text" class="form-control @if ($errors->has('contact_person')) is-invalid @endif" name="contact_person" tabindex="1" value="{{ $warehouse->contact_person }}">
              @if ($errors->has('contact_person'))
                <div class="invalid-feedback">
                  {{ $errors->first('contact_person') }}
                </div>
              @endif
            </div>

            <div class="form-group {{ $errors->has('address') ? ' has-error' : '' }}">
              <label for="address">Alamat</label>
              <input id="address" type="text" class="form-control @if ($errors->has('address')) is-invalid @endif" name="address" tabindex="1" value="{{ $warehouse->address }}">
              @if ($errors->has('address'))
                <div class="invalid-feedback">
                  {{ $errors->first('address') }}
                </div>
              @endif
            </div>

            <label for="address">Cari Lokasi / (Geser Marker di Maps)</label>
            <div class="input-group mb-4">
              <input type="text" id="search_location" class="form-control">
              <div class="input-group-append">
                <button class="btn btn-primary get_map">Cari</button>
              </div>
            </div>

            <div id="geomap" data-height="400" class="mb-4"></div>

            <div class="row">
              <div class="col-lg-6">
                <div class="form-group {{ $errors->has('lat') ? ' has-error' : '' }}">
                  <label for="lat">Latitude</label>
                  <input id="input_latitude" type="text" class="form-control @if ($errors->has('lat')) is-invalid @endif" name="lat" tabindex="1" value="{{ $warehouse->lat }}" readonly="true">
                  @if ($errors->has('lat'))
                    <div class="invalid-feedback">
                      {{ $errors->first('lat') }}
                    </div>
                  @endif
                </div>
              </div>
              <div class="col-lg-6">
                <div class="form-group {{ $errors->has('lng') ? ' has-error' : '' }}">
                  <label for="lng">Longitude</label>
                  <input id="input_longitude" type="text" class="form-control @if ($errors->has('lng')) is-invalid @endif" name="lng" tabindex="1" value="{{ $warehouse->lng }}" readonly="true">
                  @if ($errors->has('lng'))
                    <div class="invalid-feedback">
                      {{ $errors->first('lng') }}
                    </div>
                  @endif
                </div>
              </div>
            </div>

            <div class="form-group">
              <button type="submit" class="btn btn-primary btn-block" tabindex="4">
                Simpan
              </button>
            </div>
          </form>

        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?libraries=places&key={{ env("MAPS_API_KEY", "PUT_YOUR_API_KEY") }}"></script>
  <script type="text/javascript" src="{{ asset('vendor/sb-temp/vendor/maps/gmaps.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/maps-config.js') }}"></script>
@endsection
