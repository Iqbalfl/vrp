@extends('layouts.main')

@section('content')
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Sales Order</h1>
  </div>

  <div class="row">
    <div class="col-lg-12">

      <!-- Basic Card Example -->
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Edit Sales Order</h6>
        </div>
        <div class="card-body">
          
          <form method="POST" action="{{ route('sales-order.update', $sales_order->id) }}" enctype="multipart/form-data">
            @csrf
            @method('PUT')

            <div class="form-group {{ $errors->has('so_number') ? ' has-error' : '' }}">
              <label for="so_number">SO Number</label>
              <input id="so_number" type="text" class="form-control @if ($errors->has('so_number')) is-invalid @endif" name="so_number" tabindex="1" value="{{ $sales_order->so_number }}" readonly="tr">
              @if ($errors->has('so_number'))
                <div class="invalid-feedback">
                  {{ $errors->first('so_number') }}
                </div>
              @endif
            </div>

            <div class="form-group {{ $errors->has('customer_id') ? ' has-error' : '' }}">
              <label for="customer_id">Customer</label>
              <select id="customer_id" class="form-control @if ($errors->has('customer_id')) is-invalid @endif" name="customer_id" tabindex="1">
                <option value=""></option>
                @foreach (\App\Customer::all() as $item)
                  <option value="{{ $item->id }}" @if ($sales_order->customer_id == $item->id) selected @endif>{{ $item->name }}</option>
                @endforeach
              </select>
              @if ($errors->has('customer_id'))
                <div class="invalid-feedback">
                  {{ $errors->first('customer_id') }}
                </div>
              @endif
            </div>

            <div class="row">
              <div class="col-md-6">
                <div class="form-group {{ $errors->has('quantity') ? ' has-error' : '' }}">
                  <label for="quantity">Quantity</label>
                  <div class="input-group mb-2">
                    <input id="quantity" type="number" class="form-control @if ($errors->has('quantity')) is-invalid @endif" name="quantity" tabindex="1" value="{{ $sales_order->quantity }}">
                    <div class="input-group-append">
                      <div class="input-group-text">unit</div>
                    </div>
                  </div>
                  @if ($errors->has('quantity'))
                    <div class="invalid-feedback">
                      {{ $errors->first('quantity') }}
                    </div>
                  @endif
                </div>
              </div>
              
              <div class="col-md-6">
                <div class="form-group {{ $errors->has('volume') ? ' has-error' : '' }}">
                  <label for="volume">Volume</label>              
                  <div class="input-group mb-2">
                    <input id="volume" type="number" class="form-control @if ($errors->has('volume')) is-invalid @endif" name="volume" tabindex="1" value="{{ $sales_order->volume }}">
                    <div class="input-group-append">
                      <div class="input-group-text">cm<sup>3</sup></div>
                    </div>
                  </div>
                  @if ($errors->has('volume'))
                    <div class="invalid-feedback">
                      {{ $errors->first('volume') }}
                    </div>
                  @endif
                </div>
              </div>
            </div>

            <div class="form-group {{ $errors->has('warehouse_id') ? ' has-error' : '' }}">
              <label for="warehouse_id">Dari Warehouse</label>
              <select id="warehouse_id" class="form-control @if ($errors->has('warehouse_id')) is-invalid @endif" name="warehouse_id" tabindex="1">
                <option value=""></option>
                @foreach (\App\Warehouse::all() as $item)
                  <option value="{{ $item->id }}" @if ($sales_order->warehouse_id == $item->id) selected @endif>{{ $item->name }}</option>
                @endforeach
              </select>
              @if ($errors->has('warehouse_id'))
                <div class="invalid-feedback">
                  {{ $errors->first('warehouse_id') }}
                </div>
              @endif
            </div>

            <div class="form-group {{ $errors->has('delivery_date') ? ' has-error' : '' }}">
              <label for="delivery_date">Delivery Date</label>
              <input id="delivery_date" type="date" class="form-control @if ($errors->has('delivery_date')) is-invalid @endif" name="delivery_date" tabindex="1" value="{{ $sales_order->delivery_date }}">
              @if ($errors->has('delivery_date'))
                <div class="invalid-feedback">
                  {{ $errors->first('delivery_date') }}
                </div>
              @endif
            </div>

            <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
              <label for="description">Keterangan</label>
              <input id="description" type="text" class="form-control @if ($errors->has('description')) is-invalid @endif" name="description" tabindex="1" value="{{ $sales_order->description }}">
              @if ($errors->has('description'))
                <div class="invalid-feedback">
                  {{ $errors->first('description') }}
                </div>
              @endif
            </div>

            <div class="form-group">
              <button type="submit" class="btn btn-primary btn-block" tabindex="1">
                Simpan
              </button>
            </div>
          </form>

        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
  <script type="text/javascript">
    $(document).ready(function() {
      $('#customer_id').select2({
        placeholder: "Pilih Customer"
      });

      $('#warehouse_id').select2({
        placeholder: "Pilih Warehouse"
      });
    });
  </script>
@endsection
