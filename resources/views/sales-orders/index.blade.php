@extends('layouts.main')

@section('content')
  <div class="d-sm-flex align-items-center justify-content-start mb-4">
    <h1 class="h3 mb-0 text-gray-800">Sales Order</h1>
    <a class="btn btn-sm btn-primary mr-auto ml-4" href="{{ route('sales-order.create') }}"><i class="fa fa-plus"></i> Tambah Data</a> 
  </div>

  <div class="row">
    <div class="col-lg-12">

      <!-- Basic Card Example -->
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">List Sales Order</h6>
        </div>
        <div class="card-body">

        <div class="table-responsive">
          <table class="table table-hover table-bordered datatable">
            <thead>                                 
              <tr>
                <th>#</th>
                <th>SO Number</th>
                <th>Customer</th>
                <th>Quantity</th>
                <th>Volume (cm<sup>3</sup>)</th>
                <th>Date</th>
                <th>Status</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody></tbody>
          </table>
        </div>

        </div>
      </div>
    </div>
  </div>
@endsection
@section('script')
<script>
  $(document).ready(function() {
      $('.datatable').DataTable({
          processing: true,
          serverSide: true,
          autoWidth: false,
          language: {
              url: '{{ asset('vendor/sb-temp/vendor/datatables/lang/Indonesian.json') }}'
          },
          ajax: {
            url: '{{ route('sales-order.index') }}'
          },
          columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
            {data: 'so_number', name: 'so_number'},
            {data: 'customer.name', name: 'customer.name'},
            {data: 'quantity', name: 'quantity'},
            {data: 'volume', name: 'volume'},
            {data: 'delivery_date', name: 'delivery_date'},
            {data: 'display_status', name: 'display_status', orderable: false, searchable: false},
            {data: 'action', name: 'action', orderable: false, searchable: false},
          ]
      });

      $(document).on('click','.js-submit-confirm', function(e){
          e.preventDefault();
          swal({
            title: 'Apakah anda yakin ingin menghapus?',
            text: 'Data yang sudah dihapus, tidak dapat dikembalikan!',
            icon: 'warning',
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              $(this).closest('form').submit();
            } 
          });
      });
  });
</script>
@endsection
