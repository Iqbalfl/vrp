@extends('layouts.main')

@section('content')
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Sales Order</h1>
  </div>

  <div class="row">
    <div class="col-lg-12">

      <!-- Basic Card Example -->
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Detail Sales Order</h6>
        </div>
        <div class="card-body">
          
          <p>
            SO Number: <br><strong>{{$sales_order->so_number}}</strong>
            <hr>
          </p>
          <p>
            Customer: <br><strong>{{$sales_order->customer->name}}</strong>
            <hr>
          </p>
          <p>
            Alamat: <br><strong>{{$sales_order->customer->address}}</strong>
            <hr>
          </p>
          <p>
            Kecamatan: <br><strong>{{$sales_order->customer->district->name}}</strong>
            <hr>
          </p>
          <p>
            Kota / Kabupaten: <br><strong>{{$sales_order->customer->city->name}}</strong>
            <hr>
          </p>
          <p>
            Provisi: <br><strong>{{$sales_order->customer->province->name}}</strong>
            <hr>
          </p>
          <p>
            Quantity: <br><strong>{{$sales_order->quantity}} unit</strong>
            <hr>
          </p>
          <p>
            Volume: <br><strong>{{$sales_order->volume}} cm<sup>3</sup></strong>
            <hr>
          </p>
          <p>
            Delivery Date: <br><strong>{{$sales_order->delivery_date}}</strong>
            <hr>
          </p>
          <p>
            Keterangan: <br><strong>{{$sales_order->description}}</strong>
            <hr>
          </p>
          <p>
            Status: <br><strong>{{$sales_order->display_status}}</strong>
            <hr>
          </p>

        </div>
      </div>
    </div>
  </div>
@endsection