<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>{{config('app.name')}}</title>
  
  <!-- Custom fonts for this template-->
  <link href="{{ asset('vendor/sb-temp/vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="{{ asset('vendor/sb-temp/css/sb-admin-2.min.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="{{ asset('vendor/sb-temp/vendor/datatables/dataTables.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('vendor/sb-temp/vendor/toastr/build/toastr.min.css') }}">
  <link rel="stylesheet" href="{{ asset('vendor/sb-temp/vendor/daterangepicker/daterangepicker.css') }}">
  <link rel="stylesheet" href="{{ asset('vendor/sb-temp/vendor/select2/dist/css/select2.min.css') }}">

  <link href="{{ asset('vendor/sb-temp/css/custom.css') }}" rel="stylesheet">
  @yield('css')


</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ route('home') }}">
        <div class="sidebar-brand-icon">
          <i class="fas fa-cogs"></i>
        </div>
        <div class="sidebar-brand-text mx-3">
          @php
            $arr = explode(' ', config('app.name'));
            $initial = '';
            foreach($arr as $a) {
              $initial .= substr($a, 0, 1);
            }
            echo $initial;
          @endphp
        </div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="{{ route('home') }}">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span>
        </a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

        <!-- Heading -->
        <div class="sidebar-heading">
          Menu Utama
        </div>

        <!-- Nav Item - Tables -->
        <li class="nav-item">
          <a class="nav-link" href="{{ route('warehouse.index') }}">
            <i class="fas fa-fw fa-warehouse"></i>
            <span>Warehouse</span>
          </a>
        </li>

        <!-- Nav Item - Tables -->
        <li class="nav-item">
          <a class="nav-link" href="{{ route('inventory.index') }}">
            <i class="fas fa-fw fa-box"></i>
            <span>Inventory</span>
          </a>
        </li>

        <!-- Nav Item - Tables -->
        <li class="nav-item">
          <a class="nav-link" href="{{ route('customer.index') }}">
            <i class="fas fa-fw fa-users"></i>
            <span>Customer</span>
          </a>
        </li>

        <!-- Nav Item - Tables -->
        <li class="nav-item">
          <a class="nav-link" href="{{ route('transportation.index') }}">
            <i class="fas fa-fw fa-truck"></i>
            <span>Moda Transportasi</span>
          </a>
        </li>

        <!-- Nav Item - Tables -->
        <li class="nav-item">
          <a class="nav-link" href="{{ route('sales-order.index') }}">
            <i class="fas fa-fw fa-chart-area"></i>
            <span>Sales Order</span>
          </a>
        </li>
        
        <!-- Nav Item - Tables -->
        <li class="nav-item">
          <a class="nav-link" href="{{ route('store.index') }}">
            <i class="fas fa-fw fa-shopping-cart"></i>
            <span>Store</span>
          </a>
        </li>

        <!-- Nav Item - Tables -->
        <li class="nav-item">
          <a class="nav-link" href="{{ route('logistic.index') }}">
            <i class="fas fa-fw fa-road"></i>
            <span>Logistic</span>
          </a>
        </li>

        <!-- Nav Item - Tables -->
        <li class="nav-item">
          <a class="nav-link" href="{{ route('routing.index') }}">
            <i class="fas fa-fw fa-route"></i>
            <span>Routing</span>
          </a>
        </li>

        <!-- Nav Item - Tables -->
        <li class="nav-item">
          <a class="nav-link" href="{{ route('manager.index') }}">
            <i class="fas fa-fw fa-tasks"></i>
            <span>Manager</span>
          </a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider">

        <!-- Heading -->
        <div class="sidebar-heading">
          Akun
        </div>

        <!-- Nav Item - Tables -->
        <li class="nav-item">
          <a class="nav-link" href="{{ route('profile.show') }}">
            <i class="fas fa-fw fa-user"></i>
            <span>Profil</span>
          </a>
        </li>

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Search -->
          <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
            <div class="input-group">
              <input type="text" class="form-control bg-light border-0 small" placeholder="Pencarian" aria-label="Search" aria-describedby="basic-addon2">
              <div class="input-group-append">
                <button class="btn btn-primary" type="button" style="height: 34px;">
                  <i class="fas fa-search fa-sm"></i>
                </button>
              </div>
            </div>
          </form>
          
          <!-- main title -->
          <h4>{{ Auth::user()->display_role }}</h4>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <div class="topbar-divider d-none d-sm-block"></div>
            
            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small">{{ Auth::user()->name }}</span>
                @if (is_null(Auth::user()->avatar))
                  <img class="img-profile rounded-circle" src="{{asset('vendor/sb-temp/img/avatar-1.jpg')}}">
                @else
                  <img class="img-profile rounded-circle" src="{{asset('uploads/images/avatars/'.Auth::user()->avatar)}}">
                @endif
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="{{ route('profile.show') }}">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Profile
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="{{ route('logout') }}" data-toggle="modal" onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  {{ csrf_field() }}
              </form>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          @yield('content')

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; {{config('app.name')}} 2019</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Bootstrap core JavaScript-->
  <script src="{{ asset('vendor/sb-temp/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{ asset('vendor/sb-temp/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

   <!-- Core plugin JavaScript-->
  <script src="{{ asset('vendor/sb-temp/vendor/datatables/jquery.dataTables.min.js') }} "></script>
  <script src="{{ asset('vendor/sb-temp/vendor/datatables/dataTables.bootstrap4.min.js') }} "></script>
  <script src="{{ asset('vendor/sb-temp/vendor/jquery-easing/jquery.easing.min.js')}}"></script>
  <script src="{{ asset('vendor/sb-temp/vendor/toastr/build/toastr.min.js') }} "></script>
  <script src="{{ asset('vendor/sb-temp/vendor/daterangepicker/moment.min.js') }} "></script>
  <script src="{{ asset('vendor/sb-temp/vendor/daterangepicker/daterangepicker.js') }} "></script>
  <script src="{{ asset('vendor/sb-temp/vendor/sweetalert/sweetalert.min.js') }} "></script>
  <script src="{{ asset('vendor/sb-temp/vendor/select2/dist/js/select2.min.js') }}"></script>

  <!-- Custom scripts for all pages-->
  <script src="{{ asset('vendor/sb-temp/js/sb-admin-2.min.js')}}"></script>

  @include('partials._toast')
  @yield('script')

</body>

</html>
