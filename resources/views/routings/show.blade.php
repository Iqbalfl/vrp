@extends('layouts.main')

@section('content')
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Routing</h1>
  </div>

  <div class="row">
    <div class="col-lg-12">

      <!-- Basic Card Example -->
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Detail Routing</h6>
        </div>
        <div class="card-body">
          
          <p>
            Route : <br><strong>{{$routing->route}}</strong>
            <hr>
          </p>
          <p>
            Total Jarak : <br><strong>{{$routing->total_distance}} Km</strong>
            <hr>
          </p>
          <p>
            Total Waktu Tempuh : <br><strong>{{$routing->total_duration}} Menit</strong>
            <hr>
          </p>
          <p>
            Total Volume Barang : <br><strong>{{$routing->total_volume}} cm<sup>3</sup></strong>
            <hr>
          </p>
          <p>
            Tanggal Delivery : <br><strong>{{$routing->delivery_date}}</strong>
            <hr>
          </p>
          <p>
            Truck : <br><strong>{{$routing->transportation->name}}</strong>
            <hr>
          </p>

        </div>
      </div>
    </div>

    <div class="col-lg-12">

      <!-- Basic Card Example -->
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Sales Order</h6>
        </div>
        <div class="card-body">
          
          <div class="table-responsive">
            <table class="table table-hover table-bordered">
              <thead>                                 
                <tr>
                  <th>#</th>
                  <th>Nama Customer</th>
                  <th>Sales Order Number</th>
                  <th>Quantity</th>
                  <th>Volume Order (cm<sup>3</sup>)</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($routing->details as $key => $item)
                  <tr>
                    <td>{{++$key}}</td>
                    <td>{{$item->customer_name}}</td>
                    <td>{{$item->salesOrder->so_number}}</td>
                    <td>{{$item->salesOrder->quantity}}</td>
                    <td>{{$item->volume}}</td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>

        </div>
      </div>
    </div>

  </div>
@endsection