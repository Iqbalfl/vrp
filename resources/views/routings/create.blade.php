@extends('layouts.main')

@section('content')
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Route</h1>
  </div>

  <div class="row">

    <div class="col-lg-8">
      <!-- Basic Card Example -->
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Generate Route</h6>
        </div>
        <div class="card-body">
          @if ($errors->any())
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
              <strong>Perhatian</strong><br>
              @foreach ($errors->all() as $error)
                  {{ $error }}<br>
              @endforeach
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          @endif
          
          <form id="form-route" method="POST" action="{{ route('routing.store') }}" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="form-group">
              <label for="transportation_id">Pilih Truck <br><span class="badge badge-success">Tersedia {{\App\Transportation::available()->count()}}</span></label>
              <select id="transportation_id" class="form-control" name="transportation_id" tabindex="1">
                <option value=""></option>
                @foreach (\App\Transportation::available()->get() as $item)
                  <option data-value="{{$item->body_volume}}" value="{{ $item->id }}">{{ $item->name }} | Kapasitas Angkut : {{$item->body_volume}} cm<sup>3</sup></option>
                @endforeach
              </select>
            </div>

            <div class="form-group">
              <label for="so_id">Pilih Sales Order <br><span class="badge badge-success">Tersedia {{\App\SalesOrder::available()->count()}}</span></label>
              @foreach (\App\SalesOrder::available()->get() as $item)
                <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="check{{$item->id}}" name="so_id[]" value="{{ $item->id }}" data-value="{{$item->volume}}">
                  <label class="custom-control-label" for="check{{$item->id}}">Customer : <b>{{$item->customer->name}}</b> | Quantity : <b>{{$item->quantity}}</b> | Volume : <b>{{$item->volume}} cm<sup>3</sup></b></label>
                </div>
              @endforeach
            </div>

            <div class="form-group">
              <button type="submit" class="btn btn-primary btn-block" tabindex="1">
                Generate
              </button>
            </div>
          </form>

        </div>
      </div>
    </div>

    <div class="col-lg-4">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Rincian</h6>
        </div>
        <div class="card-body">
        <p>
          <b>Kapasitas Truck</b><br> <span class="capacity-value">0</span> cm<sup>3</sup>
        </p>

        <p>
          <b>Total Order Volume</b><br> <span class="total-volume">0</span> cm<sup>3</sup>
        </p>

        <hr>

        <p>
          <b>Kapasitas Tersedia</b><br> <span class="available">0</span> cm<sup>3</sup>
        </p>

        <hr>
        <small><i>*Catatan : Kapasitas order tidak bisa melebihi kapasitas angkut truck.</i></small>

        </div>
      </div>
    </div>
    

  </div>
@endsection

@section('script')
  <script type="text/javascript">
    $(document).ready(function() {
      $('#transportation_id').select2({
        placeholder: "Pilih Truck"
      });
    });

    $(document).ready(function() {
     
      var orderVolume = 0;
      var capacityValue = 0;

      $('#transportation_id').on('change', function() {
        var id = $(this).val();
        capacityValue = $('#transportation_id').find(':selected').data('value');
        $('.capacity-value').text(capacityValue);
        calculateCap();
      });

      $("input[type='checkbox']").change(function() {
        orderVolume = 0;
        $('input[type="checkbox"]:checked').each(function(index, elem) {
           orderVolume += parseInt($(elem).data('value'));
        });
         
        $('.total-volume').html(orderVolume);
        calculateCap();
      });

      function calculateCap() {
        var result;
        result = capacityValue - orderVolume;
        $('.available').html(result);
      }
    });

    $("#form-route").submit(function(){

      if ($('.available').html() < 0) {
        alert('Kapasitas order melebihi batas angkut!');
        return false
      }

      return true;
    });
  </script>
@endsection
