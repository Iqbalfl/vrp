@extends('layouts.main')

@section('content')
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Logistic</h1>
  </div>

  <div class="row">
    <div class="col-lg-12">

      <!-- Basic Card Example -->
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Tambah Logistic</h6>
        </div>
        <div class="card-body">
          
          <form method="POST" action="{{ route('logistic.store') }}" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="form-group {{ $errors->has('origin_id') ? ' has-error' : '' }}">
              <label for="origin_id">Origin</label>
              <select id="origin_id" class="form-control @if ($errors->has('origin_id')) is-invalid @endif" name="origin_id" tabindex="1">
                <option value=""></option>
                <optgroup label="Warehouse">
                  @foreach (\App\Warehouse::all() as $item)
                    <option value="{{ 'wh'.$item->id }}">{{ $item->name }}</option>
                  @endforeach
                </optgroup>
                <optgroup label="Customer">
                  @foreach (\App\Customer::all() as $item)
                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                  @endforeach
                </optgroup>
              </select>
              @if ($errors->has('origin_id'))
                <div class="invalid-feedback">
                  {{ $errors->first('origin_id') }}
                </div>
              @endif
            </div>

            <div class="form-group {{ $errors->has('destination_id') ? ' has-error' : '' }}">
              <label for="destination_id">Destination</label>
              <select id="destination_id" class="form-control @if ($errors->has('destination_id')) is-invalid @endif" name="destination_id" tabindex="1">
                <option value=""></option>
                <optgroup label="Customer">
                  @foreach (\App\Customer::all() as $item)
                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                  @endforeach
                </optgroup>
              </select>
              @if ($errors->has('destination_id'))
                <div class="invalid-feedback">
                  {{ $errors->first('destination_id') }}
                </div>
              @endif
            </div>

            <div class="form-group">
              <button type="submit" class="btn btn-primary btn-block" tabindex="1">
                Simpan
              </button>
            </div>
          </form>

        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
  <script type="text/javascript">
    $(document).ready(function() {
      $('#origin_id').select2({
        placeholder: "Pilih Origin"
      });

      $('#destination_id').select2({
        placeholder: "Pilih Destination"
      });
    });
  </script>
@endsection
