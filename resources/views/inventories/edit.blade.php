@extends('layouts.main')

@section('content')
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Inventory</h1>
  </div>

  <div class="row">
    <div class="col-lg-12">

      <!-- Basic Card Example -->
      <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Ubah Inventory</h6>
        </div>
        <div class="card-body">
          
          <form method="POST" action="{{ route('inventory.update', $inventory->id) }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('PUT') }}

            <div class="form-group {{ $errors->has('warehouse_id') ? ' has-error' : '' }}">
              <label for="warehouse_id">Warehouse</label>
              <select id="warehouse_id" class="form-control @if ($errors->has('warehouse_id')) is-invalid @endif" name="warehouse_id" tabindex="1">
                <option value=""></option>
                @foreach (\App\Warehouse::all() as $item)
                  <option value="{{ $item->id }}" @if ($inventory->warehouse_id == $item->id) selected @endif>{{ $item->name }}</option>
                @endforeach
              </select>
              @if ($errors->has('warehouse_id'))
                <div class="invalid-feedback">
                  {{ $errors->first('warehouse_id') }}
                </div>
              @endif
            </div>

            <div class="form-group {{ $errors->has('quantity') ? ' has-error' : '' }}">
              <label for="quantity">Jumlah</label>
              <input id="quantity" type="number" class="form-control @if ($errors->has('quantity')) is-invalid @endif" name="quantity" tabindex="1" value="{{ $inventory->quantity }}">
              @if ($errors->has('quantity'))
                <div class="invalid-feedback">
                  {{ $errors->first('quantity') }}
                </div>
              @endif
            </div>

            <div class="form-group">
              <button type="submit" class="btn btn-primary btn-block" tabindex="1">
                Simpan
              </button>
            </div>
          </form>

        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
  <script type="text/javascript">
    $(document).ready(function() {
      $('#warehouse_id').select2({
        placeholder: "Pilih Warehouse"
      });
    });
  </script>
@endsection
