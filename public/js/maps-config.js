var geocoder;
var map;
var marker;

/*
 * Google Map with marker
 */
function initialize() {
  var initialLat = $('#input_latitude').val();
  var initialLong = $('#input_longitude').val();
  initialLat = initialLat?initialLat:-6.9174639;
  initialLong = initialLong?initialLong:107.61912280000001;

  var latlng = new google.maps.LatLng(initialLat, initialLong);
  var options = {
    zoom: 16,
    center: latlng,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };

  map = new google.maps.Map(document.getElementById("geomap"), options);

  geocoder = new google.maps.Geocoder();

  marker = new google.maps.Marker({
    map: map,
    draggable: true,
    position: latlng
  });

  google.maps.event.addListener(marker, "dragend", function () {
    var point = marker.getPosition();
    map.panTo(point);
    geocoder.geocode({'latLng': marker.getPosition()}, function (results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        map.setCenter(results[0].geometry.location);
        marker.setPosition(results[0].geometry.location);
        /*$('.search_addr').val(results[0].formatted_address);*/
        $('#input_latitude').val(marker.getPosition().lat());
        $('#input_longitude').val(marker.getPosition().lng());
      }
    });
  });
}

$(document).ready(function () {
  //load google map
  initialize();
  
  /*
   * autocomplete location search
   */
  
  var inputId = document.getElementById('search_location');
  var options = {
    componentRestrictions: {
      country: 'id'
    }
  };

  var autocomplete = new window.google.maps.places.Autocomplete(inputId, options);
    google.maps.event.addListener(autocomplete, 'place_changed', function () {
    var place = autocomplete.getPlace();
    var lat = place.geometry.location.lat();
    var lng = place.geometry.location.lng();
    /*alert(lat + ", " + long);*/
    $('#input_latitude').val(lat);
    $('#input_longitude').val(lng);
    var latlng = new google.maps.LatLng(lat, lng);
    marker.setPosition(latlng);
    initialize();
  });
  
  /*
   * Point location on google map
   */
  $('.get_map').click(function (e) {
    var address = $('#search_location').val();
    geocoder.geocode({'address': address}, function (results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        map.setCenter(results[0].geometry.location);
        marker.setPosition(results[0].geometry.location);
        /*$('.search_addr').val(results[0].formatted_address);*/
        $('#input_latitude').val(marker.getPosition().lat());
        $('#input_longitude').val(marker.getPosition().lng());
      } else {
        alert("Error! Something Wrong: " + status);
      }
    });
    e.preventDefault();
  });

  //Add listener to marker for reverse geocoding
  google.maps.event.addListener(marker, 'drag', function () {
    geocoder.geocode({'latLng': marker.getPosition()}, function (results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        if (results[0]) {
          /*$('.search_addr').val(results[0].formatted_address);*/
          $('#input_latitude').val(marker.getPosition().lat());
          $('#input_longitude').val(marker.getPosition().lng());
        }
      }
    });
  });
});